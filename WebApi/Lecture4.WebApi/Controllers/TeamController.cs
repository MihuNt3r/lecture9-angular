﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using Lecture4.Models.DTOs;
using Lecture4.Services;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;


namespace Lecture4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _service;

        public TeamController(TeamService service)
        {
            _service = service;
        }

        // GET: api/Team
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeams()
        {
            var teamDTOs = await _service.GetAllAsync();

            return Ok(teamDTOs);
        }

        // GET: api/Team/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TeamDTO>> GetTeam(int id)
        {
            TeamDTO teamDTO;
            try
            {
                teamDTO = await _service.GetByIdAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(teamDTO);
        }

        // PUT: api/Team/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Put([FromBody] TeamDTO teamDTO)
        {
            if (teamDTO == null)
                return BadRequest();

            try
            {
                await _service.UpdateAsync(teamDTO);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }

        // POST: api/Team
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Post([FromBody] TeamDTO teamDTO)
        {
            if (teamDTO == null)
                return BadRequest();

            if (teamDTO.Id < 0)
                return BadRequest("Wrong request body");

            try
            {
                await _service.AddAsync(teamDTO);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created("", teamDTO);
        }

        // DELETE: api/Team/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (DbUpdateException)
            {
                return Conflict("Cascade deleting is not supported");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}
