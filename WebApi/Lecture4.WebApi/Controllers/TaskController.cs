﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using Lecture4.Models.DTOs;
using Lecture4.Services;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Lecture4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _service;

        public TaskController(TaskService service)
        {
            _service = service;
        }

        // GET: api/Task
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasks()
        {
            var taskDTOs = await _service.GetAllAsync();

            return Ok(taskDTOs);
        }

        [HttpGet("notfinishedtasks")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetNotFinishedTasks()
        {
            var notFinishedTasks = await _service.GetAllNotFinishedTasksAsync();

            return Ok(notFinishedTasks);
        }

        [HttpGet("marktaskasfinished/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<int>> MarkTaskAsFinished(int id)
        {
            try
            {
                await _service.MarkTaskAsFinishedAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(id);
        }

        // GET: api/Task/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TaskDTO>> GetTask(int id)
        {
            TaskDTO taskDTO;
            try
            {
                taskDTO = await _service.GetByIdAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(taskDTO);
        }

        // PUT: api/Task/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Put([FromBody] TaskDTO taskDTO)
        {
            if (taskDTO == null)
                return BadRequest();

            try
            {
                await _service.UpdateAsync(taskDTO);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }

        // POST: api/Task
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Post([FromBody] TaskDTO taskDTO)
        {
            if (taskDTO == null)
                return BadRequest();

            if (taskDTO.Id < 0)
                return BadRequest("Wrong request body");

            try
            {
                await _service.AddAsync(taskDTO);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created("", taskDTO);
        }

        // DELETE: api/Task/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (DbUpdateException)
            {
                return Conflict("Cascade deleting is not supported");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }


            return NoContent();
        }
    }
}
