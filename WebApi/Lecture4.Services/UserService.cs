﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using Task = System.Threading.Tasks.Task;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;


namespace Lecture4.Services
{
    public class UserService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public UserService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await _context.Users.ToListAsync();

            List<UserDTO> userDTOs = _mapper.Map<List<UserDTO>>(users);

            return userDTOs;
        }

        public async Task<UserDTO> GetByIdAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var user = await _context.Users.FindAsync(id);

            if (user == null)
                throw new Exception("Can't find user with such id");

            UserDTO userDTO = _mapper.Map<UserDTO>(user);

            return userDTO;
        }

        public async Task AddUserToTeamAsync(int userId, int teamId)
        {
            var user = await _context.Users.FindAsync(userId);

            if (user == null)
                throw new Exception("Can't find user with such id");

            Team team = await _context.Teams.FindAsync(teamId);

            if (team == null)
                throw new Exception("Can't find team with such id");

            user.Team = team;
            _context.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task AddAsync(UserDTO userDTO)
        {
            if (userDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var user = _mapper.Map<User>(userDTO);

            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);

            _context.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var user = await _context.Users.FindAsync(id);

            if (user == null)
                throw new Exception("Can't find user with such id");

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }
    }
}
