﻿namespace Lecture4.Models.AuxiliaryModels
{
    public class AddToTeamModel
    {
        public int UserId { get; set; }
        public int TeamId { get; set; }
    }
}
