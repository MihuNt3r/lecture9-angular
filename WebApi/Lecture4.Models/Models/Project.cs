﻿using System;
using System.Collections.Generic;

namespace Lecture4.Models.Models
{
    public class Project
    {
        public int Id { get; set; }
        public int? AuthorId { get; set; }
        public User Author { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public List<Task> Tasks {get; set;}
        public Project()
        {
            Tasks = new List<Task>();
        }
    }
}
