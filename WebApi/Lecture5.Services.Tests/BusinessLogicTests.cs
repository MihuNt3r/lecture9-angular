using System;
using Xunit;
using Lecture4.DataAccess;
using Lecture4.Models.DTOs;
using Lecture4.Models.Models;
using Lecture4.Services;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Task = System.Threading.Tasks.Task;
using TaskEntity = Lecture4.Models.Models.Task;
using System.Threading;

namespace Lecture5.Services.Tests
{
    public class BusinessLogicTests
    {
        private readonly IMapper _mapper;

        public BusinessLogicTests()
        {
            _mapper = MapperConfiguration().CreateMapper();
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TeamDTO, Team>();
                cfg.CreateMap<Team, TeamDTO>();

                cfg.CreateMap<ProjectDTO, Project>();
                cfg.CreateMap<Project, ProjectDTO>();

                cfg.CreateMap<UserDTO, User>();
                cfg.CreateMap<User, UserDTO>();

                cfg.CreateMap<TaskDTO, TaskEntity>();
                cfg.CreateMap<TaskEntity, TaskDTO>();
            });

            return config;
        }

        #region Main Tests
        //Main tests 
        //Add user: good case
        //Get user by id: good and bad case
        //Get project by id: good and bad case
        //Get task by id: good and bad case
        //Get team by id: good and bad case
        //Delete user: good and bad case
        //Delete project: good and bad case
        //Delete task: good and bad case
        

        [Fact]
        public async Task WhenAddNewUser_ThenDbContextUsersAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeDbSet);

            CancellationToken token = new CancellationToken();

            UserService service = new UserService(fakeDbContext, _mapper);
            UserDTO userDTO = new UserDTO()
            {
                Id = 1,
                FirstName = "FakeUser"
            };            
            
            //Act
            await service.AddAsync(userDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Users.AddAsync(A<User>._, token)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChangesAsync(token)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenMarkTaskAsFinished_ThenDbContextTasksFindAndUpdateIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<TaskEntity>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeDbSet);

            CancellationToken token = new CancellationToken();

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act
            await service.MarkTaskAsFinishedAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.FindAsync(A<int>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Update(A<TaskEntity>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChangesAsync(token)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenMarkTaskAsFinishedWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<TaskEntity>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeDbSet);

             TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.MarkTaskAsFinishedAsync(-100));
        }


        [Fact]
        public async Task WhenGetUserById_ThenDbContextUsersFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act
            UserDTO userDTO = await service.GetByIdAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Users.FindAsync(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenGetUserWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.GetByIdAsync(-100));
        }

        [Fact]
        public async Task WhenGetProjectById_ThenDbContextProjectsFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act
            ProjectDTO projectDTO = await service.GetByIdAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Projects.FindAsync(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenGetProjectWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.GetByIdAsync(-100));
        }

        [Fact]
        public async Task WhenGetTaskById_ThenDbContextTasksFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<TaskEntity>>();

            //A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);
            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act
            TaskDTO taskDTO = await service.GetByIdAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.FindAsync(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenGetTaskWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<TaskEntity>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.GetByIdAsync(-100));
        }

        [Fact]
        public async Task WhenGetTeamById_ThenDbContextTeamsFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act
            TeamDTO teamDTO = await service.GetByIdAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Teams.FindAsync(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenGetTeamWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.GetByIdAsync(-100));
        }

        [Fact]
        public async Task WhenDeleteUser_ThenDbContextUsersFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act
            await service.DeleteAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Users.FindAsync(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Users.Remove(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenDeleteUserWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.DeleteAsync(-100));
        }

        [Fact]
        public async Task WhenDeleteProject_ThenDbContextProjectsFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act
            await service.DeleteAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Projects.FindAsync(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Projects.Remove(A<Project>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenDeleteProjectWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.DeleteAsync(-100));
        }

        [Fact]
        public async Task WhenDeleteTask_ThenDbContextTasksFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<TaskEntity>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act
            await service.DeleteAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.FindAsync(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Tasks.Remove(A<TaskEntity>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenDeleteTasktWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<TaskEntity>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert .ThrowsAsync<ArgumentException>(() => service.DeleteAsync(-100));
        }

        #endregion

        #region Another Tests
        [Fact]
        public async Task WhenDeleteTeam_ThenDbContextTeamsFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act
            await service.DeleteAsync(1);

            //Assert
            A.CallTo(() => fakeDbContext.Teams.FindAsync(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Teams.Remove(A<Team>._)).MustHaveHappenedOnceExactly();
        }


        [Fact]
        public async Task WhenDeleteTeamtWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert .ThrowsAsync<ArgumentException>(() => service.DeleteAsync(-100));
        }

        [Fact]
        public async Task WhenAddNewProject_ThenDbContextProjectsAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeDbSet);

            CancellationToken token = new CancellationToken();

            ProjectService service = new ProjectService(fakeDbContext, _mapper);
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Id = 1,
                Name = "FakeProject"
            };

            //Act
            await service.AddAsync(projectDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Projects.AddAsync(A<Project>._, token)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChangesAsync(token)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenAddNewTask_ThenDbContextTasksAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<TaskEntity>>();
            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeDbSet);

            CancellationToken token = new CancellationToken();

            TaskService service = new TaskService(fakeDbContext, _mapper);
            TaskDTO taskDTO = new TaskDTO()
            {
                Id = 1,
                Name = "FakeTask"
            };

            //Act
            await service.AddAsync(taskDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.AddAsync(A<TaskEntity>._, token)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChangesAsync(token)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenAddNewTeam_ThenDbContextTeamsAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeDbSet);

            CancellationToken token = new CancellationToken();

            TeamService service = new TeamService(fakeDbContext, _mapper);
            TeamDTO teamDTO = new TeamDTO()
            {
                Id = 1,
                Name = "FakeTeam"
            };

            //Act
            await service.AddAsync(teamDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Teams.AddAsync(A<Team>._, token)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChangesAsync(token)).MustHaveHappenedOnceExactly();
        }
        #endregion

        #region Tests for new Api that returns not finished tasks

        [Fact]
        public async Task WhenGetNotFinishedTasks_ThenUsersFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            LinqService service = new LinqService(fakeDbContext, _mapper);

            //Act
            await service.GetNotFinishedTasks(1);

            //Assert
            A.CallTo(() => fakeDbContext.Users.FindAsync(A<int>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task WhenGetNotFinishedTasksWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            LinqService service = new LinqService(fakeDbContext, _mapper);

            //Act / Assert
            await Assert.ThrowsAsync<ArgumentException>(() => service.GetNotFinishedTasks(-100));
        }

        #endregion
    }
}
