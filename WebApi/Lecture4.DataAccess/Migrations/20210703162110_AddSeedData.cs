﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Lecture4.DataAccess.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Denesik - Greenfelder" },
                    { 2, new DateTime(2017, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Durgan Group" },
                    { 3, new DateTime(2019, 2, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kassulke LLC" },
                    { 4, new DateTime(2018, 8, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harris LLC" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 2, new DateTime(1973, 7, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Felizia.Kirilin@yahoo.com", "Felicia", "Kirilin", new DateTime(2019, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 3, new DateTime(1977, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jared_Anderson@yahoo.com", "Jared", "Anderson", new DateTime(2020, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 1, new DateTime(1953, 11, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vivian99@yahoo.com", "Vivian", "Mertz", new DateTime(2018, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), 4 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 2, new DateTime(2021, 7, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Quia et tempora hic pariatur voluptatem doloribus sunt.", "Dem", 4 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(2018, 11, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Delectus quibusdam id quia iure neque maiores molestias sed aut", null, "withdrawal contextually-based", 3, 1, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
