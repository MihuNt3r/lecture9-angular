import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APIUrl = "https://localhost:44369/api";

  constructor(private http: HttpClient) { }

  getTeamsList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl + "/team");
  }

  addTeam(val:any){
    return this.http.post(this.APIUrl + "/team", val);
  }

  updateTeam(val:any){
    return this.http.put(this.APIUrl + "/team", val);
  }

  deleteTeam(id:number){
    return this.http.delete(this.APIUrl + "/team/" + id);
  }


  getUsersList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl + "/user");
  }

  addUser(val:any){
    return this.http.post(this.APIUrl + "/user", val);
  }

  updateUser(val:any){
    return this.http.put(this.APIUrl + "/user", val);
  }

  deleteUser(id:number){
    return this.http.delete(this.APIUrl + "/user/" + id);
  }
}
