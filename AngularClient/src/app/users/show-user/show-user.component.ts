import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {

  constructor(private service:SharedService) { }

  UsersList:any=[];

  ModalTitle:string;
  ActivateAddEditTeamComp:boolean = false;
  user:any;

  ngOnInit(): void {
    this.refreshUsersList();
  }


  addClick(){
    this.user = {
      id:0,
      teamId:"",
      firstName:"",
      lastName:"",
      email:"",
      registeredAt:"",
      birthDay:""
    }
    this.ModalTitle = "Add User";
    this.ActivateAddEditTeamComp = true;
  }

  editClick(item: any){
    this.user = item;
    this.ModalTitle = "Edit User";
    this.ActivateAddEditTeamComp = true;
  }

  deleteClick(item: any){
    if(confirm('Are you sure??')){
      this.service.deleteUser(item.id).subscribe(data=>{
        alert(data.toString());
        this.refreshUsersList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditTeamComp = false;
    this.refreshUsersList();
  }

  refreshUsersList(){
    this.service.getUsersList().subscribe(data=>{
      this.UsersList = data;
    });
  }

}
