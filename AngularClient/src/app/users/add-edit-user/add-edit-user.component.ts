import { Component, OnInit, Input } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.css']
})
export class AddEditUserComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() user:any;
  id:number;
  teamId:number;
  firstName:string;
  lastName:string;
  email:string;
  registeredAt:string;
  birthDay:string;


  ngOnInit(): void {
    this.id = this.user.id;
    this.teamId = this.user.teamId;
    this.firstName = this.user.firstName;
    this.lastName = this.user.lastName;
    this.email = this.user.email;
    this.registeredAt = this.user.registeredAt;
    this.birthDay = this.user.birthDay;
  }

  addUser(){
    var val = { 
      id:this.id,
      teamId:this.teamId,
      firtName:this.firstName,
      lastName:this.lastName,
      email:this.email,
      registeredAt:this.registeredAt,
      birthDay:this.birthDay
    };
    this.service.addUser(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateDepartment(){
    var val = { 
      id:this.id,
      teamId:this.teamId,
      firtName:this.firstName,
      lastName:this.lastName,
      email:this.email,
      registeredAt:this.registeredAt,
      birthDay:this.birthDay
    };
    this.service.updateUser(val).subscribe(res=>{
    alert(res.toString());
    });
  }

}
