import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-team',
  templateUrl: './show-team.component.html',
  styleUrls: ['./show-team.component.css']
})
export class ShowTeamComponent implements OnInit {

  constructor(private service: SharedService) { }

  TeamsList:any=[];

  ModalTitle:string;
  ActivateAddEditTeamComp:boolean = false;
  team:any;

  ngOnInit(): void {
    this.refreshTeamsList();
  }


  addClick(){
    this.team = {
      id:0,
      name:"",
      createdAt:""
    }
    this.ModalTitle = "Add Team";
    this.ActivateAddEditTeamComp = true;
  }

  editClick(item: any){
    this.team = item;
    this.ModalTitle = "Edit Team";
    this.ActivateAddEditTeamComp = true;
  }

  deleteClick(item: any){
    if(confirm('Are you sure??')){
      this.service.deleteTeam(item.id).subscribe(data=>{
        alert(data.toString());
        this.refreshTeamsList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditTeamComp = false;
    this.refreshTeamsList();
  }

  refreshTeamsList(){
    this.service.getTeamsList().subscribe(data=>{
      this.TeamsList = data;
    });
  }
}
