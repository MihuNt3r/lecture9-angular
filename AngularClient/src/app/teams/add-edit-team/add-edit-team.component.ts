import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-team',
  templateUrl: './add-edit-team.component.html',
  styleUrls: ['./add-edit-team.component.css']
})
export class AddEditTeamComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() team:any;
  id:number;
  name:string;
  createdAt:string;

  ngOnInit(): void {
    this.id = this.team.id;
    this.name = this.team.name;
    this.createdAt = this.team.createdAt;
  }

  addTeam(){
    var val = {
      id: this.id,
      name: this.name,
      createdAt: this.createdAt
    };
    this.service.addTeam(val).subscribe(res =>{
      alert(res.toString());
    });
  }

  updateTeam(){
    var val = {
      id: this.id,
      name: this.name,
      createdAt: this.createdAt
    };
    this.service.updateTeam(val).subscribe(res =>{
    alert(res.toString());
    });
  }
}
