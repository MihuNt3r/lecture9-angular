import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TeamsComponent } from './teams/teams.component';
import { ShowTeamComponent } from './teams/show-team/show-team.component';
import { AddEditTeamComponent } from './teams/add-edit-team/add-edit-team.component';
import { ShowUserComponent } from './users/show-user/show-user.component';
import { AddEditUserComponent } from './users/add-edit-user/add-edit-user.component';
import { SharedService } from './shared.service';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamsComponent,
    ShowTeamComponent,
    AddEditTeamComponent,
    UsersComponent,
    ShowUserComponent,
    AddEditUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
